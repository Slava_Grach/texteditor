﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TextEditor.BL
{
    public interface IFileManager
    {
        bool isExist(string filePath);
        string getContent(string filePath, Encoding encoding);
        string getContent(string filePath);
        void saveContent(string content, string filePath, Encoding encoding);
        void saveContent(string content, string filePath);
        int getSymbolCount(string content);
    }

    public class FileManager : IFileManager
    {
        private readonly Encoding _defaultEncoding = Encoding.GetEncoding(1251);

        public bool isExist (string filePath)
        {
            return File.Exists(filePath);
        }

        public string getContent (string filePath, Encoding encoding)
        {
            return File.ReadAllText(filePath, encoding);
        }

        public string getContent (string filePath)
        {
            return getContent(filePath, _defaultEncoding);
        }

        public void saveContent (string content, string filePath, Encoding encoding)
        {
            File.WriteAllText(filePath, content, encoding);
        }

        public void saveContent (string content, string filePath)
        {
            saveContent(content, filePath, _defaultEncoding);
        }

        public int getSymbolCount (string content)
        {
            return content.Length;
        }
    }
}
