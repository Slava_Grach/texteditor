﻿using TextEditor.BL;

namespace TextEditor
{
    class MainPresenter
    {
        private readonly IMainForm _view;
        private readonly IFileManager _manager;
        private readonly IMessageService _messageService;

        private string _currentFilePath;

        public MainPresenter (IMainForm view, IFileManager manager, IMessageService service)
        {
            _view = view;
            _manager = manager;
            _messageService = service;

            _view.setSymbolCount(0);

            _view.ContentChaged += _view_ContentChaged;
            _view.FileOpenClick += _view_FileOpenClick;
            _view.FileSaveClick += _view_FileSaveClick;
        }

        private void _view_FileSaveClick(object sender, System.EventArgs e)
        {
            try
            {
                string content = _view.Content;
                _manager.saveContent(content, _currentFilePath);
            }
            catch (System.Exception ex)
            {

                _messageService.ShowError(ex.Message);
                _messageService.ShowMessage("File was saved");
            }
        }

        private void _view_FileOpenClick(object sender, System.EventArgs e)
        {
            try
            {
                string filePath = _view.FilePash;

                bool isExit = _manager.isExist(filePath);

                if (!isExit)
                {
                    _messageService.ShowExclamation("File not found");
                    return;
                }

                _currentFilePath = filePath;

                string content = _manager.getContent(filePath);
                int count = _manager.getSymbolCount(content);

                _view.Content = content;
                _view.setSymbolCount(count);
            }
            catch (System.Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
            
        }

        private void _view_ContentChaged(object sender, System.EventArgs e)
        {
            string content = _view.Content;

            int count = _manager.getSymbolCount(content);

            _view.setSymbolCount(count);
        }
    }
}
